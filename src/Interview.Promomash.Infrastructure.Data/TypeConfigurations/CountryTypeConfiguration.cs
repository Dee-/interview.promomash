﻿using Interview.Promomash.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Interview.Promomash.Infrastructure.Data
{
    internal class CountryTypeConfiguration : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> builder)
        {
            builder.ToTable("Countries");
            builder.Property(x => x.Id).HasColumnName("Id");
            builder.Property(x => x.Title).HasColumnName("Title").IsRequired(true).HasMaxLength(Country.TITLE_MAX_LENGTH);

            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Title).IsUnique(true);
        }
    }
}
