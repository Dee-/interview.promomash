﻿using Interview.Promomash.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.Promomash.Infrastructure.Data
{
    internal class UserTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.Property(x => x.Id).HasColumnName("Id");
            builder.Property(x => x.CityId).HasColumnName("CityId").IsRequired(true);
            builder.Property(x => x.Email).HasColumnName("Email").IsRequired(true).HasMaxLength(User.EMAIL_MAX_LENGTH);
            builder.Property(x => x.PasswordHash).HasColumnName("PasswordHash").IsRequired(true);
            builder.Property(x => x.Salt).HasColumnName("Salt").IsRequired(true);


            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Email).IsUnique(true);
            builder.HasOne(x => x.City).WithMany(x => x.Users).HasForeignKey(x => x.CityId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
