﻿using Interview.Promomash.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Interview.Promomash.Infrastructure.Data
{
    internal class CityTypeCounfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.ToTable("Citites");
            builder.Property(x => x.Id).HasColumnName("Id");
            builder.Property(x => x.Title).HasColumnName("Title").IsRequired(true).HasMaxLength(City.TITLE_MAX_LENGHT);
            builder.Property(x => x.CountryId).HasColumnName("CountryId");

            builder.HasKey(x => x.Id);
            builder.HasIndex("Title", "CountryId").IsUnique(true);
            builder.HasOne(x => x.Country).WithMany(x => x.Citites).HasForeignKey(x => x.CountryId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
