﻿using Interview.Promomash.Core.Entities;
using Interview.Promomash.Core.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Transactions;

namespace Interview.Promomash.Infrastructure.Data
{
    /// <summary>
    /// UnitOfWork implementation by EF core
    /// </summary>
    internal sealed class EfUnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;
        private bool _disposed = false;
        private CommittableTransaction _transaction;

        public EfUnitOfWork(DbContext context)
        {
            _dbContext = context;
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity
        {
            return new EfRepository<TEntity>(_dbContext);
        }

        public void SaveChanges()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new UnitOfWorkException("SaveChanges failed. See innerException", ex);
            }
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new UnitOfWorkException("SaveChangesAsync failed. See innerException", ex);
            }
        }

        public void BeginTransaction()
        {
            if (_transaction == null)
            {
                _transaction = new CommittableTransaction();
            }
        }

        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            if (_transaction == null)
            {
                _transaction = new CommittableTransaction(new TransactionOptions { IsolationLevel = isolationLevel });
            }
        }

        public void RollBackTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Rollback();
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public void CommitTransaction()
        {
            if (_transaction != null)
            {
                _transaction.Commit();
                _transaction.Dispose();
                _transaction = null;
            }
        }

        #region IDisposable

        private void Dispose(bool disposeContext)
        {
            if (!_disposed && disposeContext)
            {
                RollBackTransaction();
                _dbContext.Dispose();
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
