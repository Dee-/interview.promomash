﻿using Interview.Promomash.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.Promomash.Infrastructure.Data
{
    internal abstract class BaseService
    {
        protected BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected IUnitOfWork UnitOfWork { get; }
    }
}
