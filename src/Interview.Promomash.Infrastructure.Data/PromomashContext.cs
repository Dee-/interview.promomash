﻿using System.Runtime.CompilerServices;
using Interview.Promomash.Core.Entities;
using Microsoft.EntityFrameworkCore;
[assembly: InternalsVisibleTo("Interview.Promomash")]
[assembly: InternalsVisibleTo("Interview.Promomash.Infrastructure.Data.EF.Migrations")]

namespace Interview.Promomash.Infrastructure.Data
{
    internal class PromomashContext : DbContext
    {
        public PromomashContext(DbContextOptions<PromomashContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<City> Citites { get; set; }

        public DbSet<Country> Countries { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CountryTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CityTypeCounfiguration());
            modelBuilder.ApplyConfiguration(new UserTypeConfiguration());
        }
    }
}
