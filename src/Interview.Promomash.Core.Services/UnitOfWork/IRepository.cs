﻿using Interview.Promomash.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interview.Promomash.Core.Services
{
    /// <summary>
    /// Repository for working with entities
    /// </summary>
    /// <typeparam name="TEntity">Entity type</typeparam>
    public interface IRepository<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// Returns Iqueryable expression
        /// </summary>
        IQueryable<TEntity> Query();

        /// <summary>
        /// Add new entity to repository (without saving)
        /// </summary>
        TEntity Add(TEntity entity);

        /// <summary>
        /// Delete entity from repository (without saving)
        /// </summary>
        void Remove(TEntity entity);

        /// <summary>
        /// Update entity in repository (without saving)
        /// </summary>
        TEntity Update(TEntity entity);
    }
}
