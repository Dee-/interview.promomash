﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.Promomash.Core.Services
{
    public sealed class UnitOfWorkException : Exception
    {
        public UnitOfWorkException()
        {
        }

        public UnitOfWorkException(string message) : base(message)
        {
        }

        public UnitOfWorkException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
