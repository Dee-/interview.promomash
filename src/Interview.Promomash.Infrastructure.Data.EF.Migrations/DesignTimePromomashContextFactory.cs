﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Interview.Promomash.Infrastructure.Data.EF.Migrations
{
    class DesignTimePromomashContextFactory : IDesignTimeDbContextFactory<PromomashContext>
    {
        public PromomashContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("dbConnection.json")
                .Build();

            var builder = new DbContextOptionsBuilder<PromomashContext>();

            var connectionString = configuration.GetConnectionString("PromomashTestConnection");

            builder.UseSqlServer(connectionString, b=>b.MigrationsAssembly("Interview.Promomash.Infrastructure.Data.EF.Migrations"));

            return new PromomashContext(builder.Options);
        }
    }
}
