﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Interview.Promomash.Infrastructure.Data")]

namespace Interview.Promomash.Core.Entities
{
    /// <summary>
    /// Database entity
    /// </summary>    
    public interface IEntity
    {

    }
}
