﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.Promomash.Core.Entities
{
    /// <summary>
    /// Country entity
    /// </summary>
    public class Country : IEntity
    {
        public const int TITLE_MAX_LENGTH = 200;


        public int Id { get; set; }

        public string Title { get; set; }


        /// <summary>
        /// Navigation property
        /// </summary>
        internal IList<City> Citites { get; set; }
    }
}
