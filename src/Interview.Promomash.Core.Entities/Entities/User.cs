﻿using System;

namespace Interview.Promomash.Core.Entities
{
    /// <summary>
    /// User entity
    /// </summary>
    public class User : IEntity
    {
        public const int EMAIL_MAX_LENGTH = 150;


        public Guid Id { get; set; }

        public string Email { get; set; }

        /// <summary>
        /// Hash code of user password
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        /// Salt for calculating hash code
        /// </summary>
        public string Salt { get; set; }

        public int CityId { get; set; }


        /// <summary>
        /// Navigation property
        /// </summary>
        public City City { get; set; }
    }
}
